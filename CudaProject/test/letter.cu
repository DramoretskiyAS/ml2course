#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define BASE_TYPE char 

__global__ void search_letter(int *c, const BASE_TYPE *a, int size)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    
	if (i < size)
		atomicAdd(&c[a[i]], 1);

}

int toMultiple(int a, int b) {
    int mod = a % b;

    if (mod != 0) { 
        mod = b - mod; 
        return a + mod; 
    } 
    return a; 
}

int main()
{
	srand(time(NULL));

	FILE *stream;
	stream = fopen("result", "w");
    int Size;
    printf("Enter line length \n");
    scanf("%d", &Size);
	
	Size = toMultiple(Size, 127);

    size_t Asize = Size * sizeof(BASE_TYPE);
    size_t Csize = 127 * sizeof(int); 

    BASE_TYPE *h_A = (BASE_TYPE *)malloc(Asize); 
	int *h_C = (int *)malloc(Csize); 
	int *h_B = (int *)malloc(Csize); 
    
    int salt = rand() % 10 + 1;
    printf("salt %d \n", salt);

    for (int i = 0; i < Size; i++) 
        switch (i % salt) {
        case 0:
            h_A[i] = 'A' + rand() % 26;
            break;
        default:
            h_A[i] = 'a' + rand() % 26;
            break;
        }
	
	fprintf (stream, "%s", h_A);
	fclose(stream);

    // printf("%s\n", h_A);
    for (int i = 0; i < 127; i++)  {
		h_C[i] = 0;
		h_B[i] = 0;
	}
    BASE_TYPE *d_A = NULL;
	int *d_C = NULL;
    cudaError_t cudaStatus;

    dim3 threadsPerBlock = dim3(127, 1);
    dim3 blocksPerGrid = dim3(Size / threadsPerBlock.x, 1);
    
	cudaEvent_t start, stop;
	clock_t start_time, end_time;

	cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		goto Error;
	}

	cudaStatus = cudaMalloc((void**)&d_C, Csize);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	cudaStatus = cudaMalloc((void**)&d_A, Asize);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}


	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	cudaEventRecord(start, 0);

	cudaStatus = cudaMemcpy(d_A, h_A, Asize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
	}

	search_letter<<<blocksPerGrid, threadsPerBlock>>>(d_C, d_A, Size);

	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "search_letter launch failed: %s\n", cudaGetErrorString(cudaStatus));
		goto Error;
	}

	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching search_letter!\n", cudaStatus);
		goto Error;
	}

	cudaStatus = cudaMemcpy(h_C, d_C, Csize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
	}

	cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    float KernelTime;
	cudaEventElapsedTime(&KernelTime, start, stop);

    printf("KernelTime: %.2f milliseconds\n", KernelTime);


	for (int ind = 0; ind < 127; ++ind)
	{
		if (h_C[ind] != 0)
            printf("%c   %d\n", (BASE_TYPE)(ind), h_C[ind]);
		// cout << (BASE_TYPE)(ind+65) << ' ' << h_C[ind+65] << endl;
	}

	cudaStatus = cudaDeviceReset();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceReset failed!");
		return 1;
	}

	printf("Test STARTED\n"); 
	start_time = clock();

	for (int i = 0; i < Size; i++) h_B[h_A[i]]++;

	for (int i = 0; i < 127; ++i) {
        if (h_B[i] != h_C[i]) {
            fprintf(stderr, "Result verification failed at element [%d]!\n", i);
            printf("h_B = %d, h_C[i] = %d\n", h_B[i], h_C[i]);
            exit(EXIT_FAILURE);
        }
    }

	for (int ind = 0; ind < 127; ++ind)
	{
		if (h_B[ind] != 0)
            printf("%c   %d\n", (BASE_TYPE)(ind), h_B[ind]);
		// cout << (BASE_TYPE)(ind) << ' ' << h_C[ind] << endl;
	}
	end_time = clock();
    printf("CPU Time: %.2f milliseconds\n", (double)(end_time - start_time) / CLOCKS_PER_SEC * 1000);
    printf("Test PASSED\n"); 

Error:
	cudaFree(d_C);
    cudaFree(d_A);

	free(h_A);  
	free(h_B);
    free(h_C);

    cudaEventDestroy(start);
    cudaEventDestroy(stop);
    
	return 0;
}
