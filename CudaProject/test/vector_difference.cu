#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <iostream>
#include <string>
#include <map>
#include <stdio.h>

#define BLOCK_SIZE 16

cudaError_t addWithCuda(int *a, int *b, int *c, int size);

#define BASE_TYPE int 

__global__ void addKernel(int *a, int *b, int *c, int size)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < size)
        c[i] = a[i] - b[i];
}

int toMultiple(int a, int b) {
    int mod = a % b;

    if (mod != 0) { 
        mod = b - mod; 
        return a + mod; 
    } 
    return a; 
}


using namespace std;

int main()
{   
    srand(time(NULL));
    BASE_TYPE arraySize;

    printf("Enter the length of the vector \n");
    scanf("%d", &arraySize);

    arraySize = toMultiple(arraySize, BLOCK_SIZE);
    
    size_t ArraySize = arraySize * sizeof(BASE_TYPE);  

    BASE_TYPE *h_A = (BASE_TYPE *)malloc(ArraySize);     
    BASE_TYPE *h_B = (BASE_TYPE *)malloc(ArraySize);     
    BASE_TYPE *h_C = (BASE_TYPE *)malloc(ArraySize);
    BASE_TYPE *h_C_2 = (BASE_TYPE *)malloc(ArraySize);

    clock_t start_time, end_time;

    for (int i = 0; i < arraySize; ++i) {
        h_A[i] = rand() % 10 + 1;
        h_B[i] = rand() % 10 + 1;
        h_C[i] = 0;
        h_C_2[i] = 0;
    }

    // Add vectors in parallel.
    cudaError_t cudaStatus = addWithCuda(h_A, h_B, h_C, arraySize);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "addWithCuda failed!");
        return 1;
    }

    printf("A: {%d,%d,%d,%d,%d}\n", h_A[0], h_A[1], h_A[2], h_A[3], h_A[4]);
    printf("B: {%d,%d,%d,%d,%d}\n", h_B[0], h_B[1], h_B[2], h_B[3], h_B[4]);
    printf("C: {%d,%d,%d,%d,%d}\n", h_C[0], h_C[1], h_C[2], h_C[3], h_C[4]);

    // cudaDeviceReset must be called before exiting in order for profiling and
    // tracing tools such as Nsight and Visual Profiler to show complete traces.
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }

    printf("Test STARTED\n"); 
	start_time = clock();
    
    for (int i = 0; i < arraySize; ++i) {
        h_C_2[i] = h_A[i] - h_B[i];
    }
    printf("C_2: {%d,%d,%d,%d,%d}\n", h_C_2[0], h_C_2[1], h_C_2[2], h_C_2[3], h_C_2[4]);
    
    for (int i = 0; i < arraySize; ++i) {
        BASE_TYPE sum = 0;
        sum = h_A[i] - h_B[i];
        if (fabs(h_C[i] - sum) > 1e-3) {
            fprintf(stderr, "Result verification failed at element [%d]!\n", i);
            printf("sum = %d, h_C[i] = %d\n", sum, h_C[i]);
            exit(EXIT_FAILURE);
        }
    }

    end_time = clock();
    printf("CPU Time: %.2f milliseconds\n", (double)(end_time - start_time) / CLOCKS_PER_SEC * 1000);
    printf("Test PASSED\n"); 

    return 0;
}

// Helper function for using CUDA to add vectors in parallel.
cudaError_t addWithCuda(int *a, int *b, int *c, int size)
{
    int *dev_a = 0;
    int *dev_b = 0;
    int *dev_c = 0;
    cudaError_t cudaStatus;

    cudaEvent_t start, stop;

    dim3 threadsPerBlock = dim3(BLOCK_SIZE, 1);
    dim3 blocksPerGrid = dim3(size / BLOCK_SIZE, 1);

    // Choose which GPU to run on, change this on a multi-GPU system.
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        goto Error;
    }

    // Allocate GPU buffers for three vectors (two input, one output)    .
    cudaStatus = cudaMalloc((void**)&dev_c, size * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    cudaStatus = cudaMalloc((void**)&dev_a, size * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    cudaStatus = cudaMalloc((void**)&dev_b, size * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    // Copy input vectors from host memory to GPU buffers.
    cudaStatus = cudaMemcpy(dev_a, a, size * sizeof(int), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

    cudaStatus = cudaMemcpy(dev_b, b, size * sizeof(int), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start, 0);

    // Launch a kernel on the GPU with one thread for each element.
    addKernel<<<blocksPerGrid, threadsPerBlock>>>(dev_a, dev_b, dev_c, size);

    // Check for any errors launching the kernel
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "addKernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
        goto Error;
    }
    
    // cudaDeviceSynchronize waits for the kernel to finish, and returns
    // any errors encountered during the launch.
    cudaStatus = cudaDeviceSynchronize();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel!\n", cudaStatus);
        goto Error;
    }

    // Copy output vector from GPU buffer to host memory.
    cudaStatus = cudaMemcpy(c, dev_c, size * sizeof(int), cudaMemcpyDeviceToHost);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    float KernelTime;
	cudaEventElapsedTime(&KernelTime, start, stop);

    printf("KernelTime: %.2f milliseconds\n", KernelTime);
	
			
Error:
    cudaFree(dev_c);
    cudaFree(dev_a);
    cudaFree(dev_b);
    
	
    return cudaStatus;
}