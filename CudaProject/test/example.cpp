#include <cstdlib>
#include <cstring>
#include <iostream>
using namespace std;
 
bool find_ch(char* str, char ch)
{
    for ( ; *str; ++str)
        if (*str == ch)
            return true;
    return false;
}
 
int main()
{
    char str[] = "asdasdasdasdasdasdasdasdasdadadsada";
   
    const int N = 256;
    int arr[N] = {0};
    for (int i = 0; i < strlen(str); ++i)
    {
        if (find_ch(str + i, str[i]))
            ++arr[str[i]];
    }
 
    for (int i = 0; i < N; ++i)
    {
        if (arr[i] != 0)
            cout << (char)i << ' ' << arr[i] << endl;
    }
        
    system("pause");
    return 0;
}
