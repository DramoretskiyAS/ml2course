#include "cuda_runtime.h" 
#include "device_launch_parameters.h" 
#include <stdio.h> 
#include <stdlib.h>
#include <time.h>


#define BLOCK_SIZE 16
#define BASE_TYPE double 

__global__ void matrixMult(const BASE_TYPE *A, const BASE_TYPE *B, BASE_TYPE *C, int Acols, int Bcols)  {
    int y = blockDim.y * blockIdx.y + threadIdx.y;
    int i0 = Acols * y;
    int j0 = blockDim.x * blockIdx.x + threadIdx.x;
    BASE_TYPE sum = 0;
    
    for (int k = 0; k < Acols; k++) 
        sum += A[i0 + k] * B[k * Bcols + j0]; 
    
    C[Bcols * y + j0] = sum;
}

int toMultiple(int a, int b) {
    int mod = a % b;

    if (mod != 0) { 
        mod = b - mod; 
        return a + mod; 
    } 
    return a; 
}

void print_matrix(int rows, int cols, BASE_TYPE *A){
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            printf("%.2f", A[i*cols + j]); 
        }
        printf("\n");
    }
    printf("\n");
}

int main() {
    cudaEvent_t start, stop; 
    cudaEventCreate(&start); 
    cudaEventCreate(&stop);

    int Arows = 5000; 
    int Acols = 5000; 
    int Brows = Acols; 
    int Bcols = 5000;

    // int Arows, Acols, Brows, Bcols;

    // printf("Enter the number of rows of matrix A \n");
    // scanf("%d", &Arows);
    // printf("Enter the number of columns of the matrix A \n");
    // scanf("%d", &Acols);

    // printf("Enter the number of columns of the matrix B \n");
    // scanf("%d", &Bcols);
    // Brows = Acols;

    Arows = toMultiple(Arows, BLOCK_SIZE);  
    printf("Arows = %d\n", Arows);

    Acols = toMultiple(Acols, BLOCK_SIZE);
    printf("Acols = %d\n", Acols);

    Brows = toMultiple(Brows, BLOCK_SIZE);     
    printf("Brows = %d\n", Brows);

    Bcols = toMultiple(Bcols, BLOCK_SIZE);     
    printf("Bcols = %d\n", Bcols); 

    size_t Asize = Arows * Acols * sizeof(BASE_TYPE);     
    size_t Bsize = Brows * Bcols * sizeof(BASE_TYPE);     
    size_t Csize = Arows * Bcols * sizeof(BASE_TYPE); 
    
    BASE_TYPE *h_A = (BASE_TYPE *)malloc(Asize);     
    BASE_TYPE *h_B = (BASE_TYPE *)malloc(Bsize);     
    BASE_TYPE *h_C = (BASE_TYPE *)malloc(Csize);

    for (int i = 0; i < Arows * Acols; ++i) {
        h_A[i] = rand() % 10 + 1;
    }

    for (int i = 0; i < Brows * Bcols; ++i) {
        h_B[i] = rand() % 10 + 1;
    }
    
    //print_matrix(Arows, Acols, h_A);
    //print_matrix(Brows, Bcols, h_B);

    BASE_TYPE *d_A = NULL;
    BASE_TYPE *d_B = NULL;   
    BASE_TYPE *d_C = NULL;
    cudaError_t cudaStatus;

    dim3 threadsPerBlock = dim3(BLOCK_SIZE, BLOCK_SIZE); 
    dim3 blocksPerGrid = dim3(Bcols / BLOCK_SIZE, Arows / BLOCK_SIZE);

    clock_t start_time, end_time;

    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        goto Error;
    }

    cudaStatus = cudaMalloc((void **)&d_A, Asize);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }
 
    cudaStatus = cudaMalloc((void **)&d_B, Bsize); 
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    cudaStatus = cudaMalloc((void **)&d_C, Csize); 
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    cudaStatus = cudaMemcpy(d_A, h_A, Asize, cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

    cudaStatus = cudaMemcpy(d_B, h_B, Bsize, cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }


    cudaEventRecord(start, 0);
    matrixMult<<<blocksPerGrid, threadsPerBlock>>>(d_A, d_B, d_C, Acols, Bcols); 
    cudaEventRecord(stop, 0); 
    cudaEventSynchronize(stop); 

    cudaStatus = cudaDeviceSynchronize();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching matrixMult!\n", cudaStatus);
        goto Error;
    }

    float KernelTime; 
    cudaEventElapsedTime(&KernelTime, start, stop); 
    printf("KernelTime: %.2f milliseconds\n", KernelTime);

    cudaStatus = cudaMemcpy(h_C, d_C, Csize, cudaMemcpyDeviceToHost);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

    // print_matrix(Arows, Bcols, h_C);

    printf("Test STARTED\n"); 
    start_time = clock();

    for (int i = 0; i < Arows; i++) {
        for (int j = 0; j < Bcols; j++) {
            BASE_TYPE sum = 0;
            for (int k = 0; k < Acols; k++) 
                sum += h_A[i * Acols + k] * h_B[k * Bcols + j]; 
            
            if (fabs(h_C[i * Bcols + j] - sum) > 1e-3) {
                fprintf(stderr, "Result verification failed at element [%d, %d]!\n", i, j);
                printf("sum = %f, h_C[i * Bcols + j] = %f\n", sum, h_C[i * Bcols + j]);
                exit(EXIT_FAILURE);
            }
        }
    }
    end_time = clock();
    printf("CPU Time: %.2f seconds\n", (double)(end_time - start_time) / CLOCKS_PER_SEC);
    printf("Test PASSED\n"); 

Error:

    cudaFree(d_A); 
    cudaFree(d_B); 
    cudaFree(d_C);   

    free(h_A);
    free(h_B);     
    free(h_C);

    cudaEventDestroy(start); 
    cudaEventDestroy(stop);

    return 0; 
}      
