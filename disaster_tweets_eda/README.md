Для запуска задания:

1. Установить зависимости и запустить ноут
```
python -m venv venv
. venv/bin/activate
pip install -r requirements.txt
jupyter notebook
```

2. Открыть disaster_tweets_eda.ipynb
