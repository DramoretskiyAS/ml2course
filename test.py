"""A test file to check if cuda works in tensorflow and torch"""
import tensorflow as tf
import torch

tf.config.list_physical_devices('GPU')

print(tf.test.is_built_with_cuda())
print(tf.test.is_gpu_available(cuda_only=True, min_cuda_compute_capability=None))
print(tf.test.is_gpu_available(cuda_only=False, min_cuda_compute_capability=None))

tf.debugging.set_log_device_placement(True)
print("Num GPUs Available: ", len(tf.config.list_physical_devices('GPU')))

print(torch.cuda.is_available())
print(torch.cuda.current_device())
print(torch.cuda.device(0))
print(torch.cuda.device_count())
print(torch.cuda.get_device_name(0))
