"""Sova testing module.

For the tests to work, you need to deploy the sova service.
For instructions on how to unfold sova service, see here https://github.com/sovaai/sova-asr
"""
import os
import re
from typing import List, Union

import pandas as pd
import requests
from datasets import load_metric, load_dataset
from tqdm import tqdm

BASE_DIR = os.path.dirname(os.path.realpath(__file__))
DIR_NAME = 'asr_public_phone_calls_2'
PATH = os.path.join(BASE_DIR, DIR_NAME)
URL = 'http://localhost:8888/asr'


LANG_ID = "ru"
CHARS_TO_IGNORE = [",", "?", "¿", ".", "!", "¡", ";", "；", ":", '""', "%", '"', "�", "ʿ", "·", "჻", "~", "՞",
                   "؟", "،", "।", "॥", "«", "»", "„", "“", "”", "「", "」", "‘", "’", "《", "》", "(", ")", "[", "]",
                   "{", "}", "=", "`", "_", "+", "<", ">", "…", "–", "°", "´", "ʾ", "‹", "›", "©", "®", "—", "→", "。",
                   "、", "﹂", "﹁", "‧", "～", "﹏", "，", "｛", "｝", "（", "）", "［", "］", "【", "】", "‥", "〽",
                   "『", "』", "〝", "〟", "⟨", "⟩", "〜", "：", "！", "？", "♪", "؛", "/", "\\", "º", "−", "^", "ʻ", "ˆ"]

test_dataset = load_dataset("common_voice", LANG_ID, split="test")

wer = load_metric("wer.py")  # https://github.com/jonatasgrosman/wav2vec2-sprint/blob/main/wer.py

chars_to_ignore_regex = f"[{re.escape(''.join(CHARS_TO_IGNORE))}]"


def audio_recognition(file_path: str, text: str) -> List[Union[str, str, bool, float]]:
    """Method for automatic speech recognition.

    Args:
        file_path: File path without extension.
        text: Text.

    Returns:
        A list containing the source text, text from the recognition service, whether the text was recognized
        correctly and the recognition time.

    """
    audio_data = open(file_path, 'rb')

    response = requests.post(URL, files={'audio_blob': (file_path, audio_data, 'application/octet-stream')})
    audio_text = response.json()['r'][0]['response'][0]['text']
    time_recognition = response.json()['r'][0]['response'][0]['time']

    text = re.sub(chars_to_ignore_regex, "", text).lower()
    correctness_of_recognition = audio_text.lower() == text

    audio_data.close()

    return [text.lower(), audio_text.lower(), correctness_of_recognition, time_recognition]


def main():
    """Performs basic logic. Search for files in a directory, run through automatic recognition and data collection.

    Returns:
        None

    """
    df = pd.DataFrame(columns=['text', 'audio_text', 'correctness_of_recognition', 'time_recognition'])

    for i, v in enumerate(tqdm(test_dataset), start=1):
        df.loc[i] = audio_recognition(v['path'], v['sentence'])

        if not i % 50000:
            df.to_csv(f'test_sova_{i}.csv', index=False)

    df.to_csv('sova_commonvoice.csv', index=False)


if __name__ == '__main__':
    main()
