"""VOSK API testing module.

For the tests to work, you need to download the required model for the Russian language from
the site https://alphacephei.com/vosk/models and unpack as "model" in the current folder.
"""
import os
import shutil
from subprocess import Popen, PIPE
import time
from typing import List, Union

import pandas as pd
from tqdm import tqdm
from vosk import Model, KaldiRecognizer, SetLogLevel

DIR_NAME = 'asr_public_phone_calls_2'

BASE_DIR = os.path.dirname(os.path.realpath(__file__))
PATH_DIR = os.path.join(BASE_DIR, DIR_NAME)
RESULT_DIR = os.path.join(BASE_DIR, f"result_{os.path.basename(__file__).split('.')[0]}")
SAMPLE_RATE = 16000

SetLogLevel(0)

if not os.path.exists('model'):
    print(
        'Please download the model from https://alphacephei.com/vosk/models and '
        'unpack as "model" in the current folder.'
    )
    exit(1)

model = Model('model')
rec = KaldiRecognizer(model, SAMPLE_RATE)


def audio_recognition(file_path: str) -> List[Union[str, str, bool, float]]:
    """Method for automatic speech recognition.

    Args:
        file_path: File path without extension.

    Returns:
        A list containing the source text, text from the recognition service, whether the text was recognized
        correctly and the recognition time.

    """
    with open(f'{file_path}.txt', 'r') as file:
        text = file.read()[:-1]

    process = Popen([
        'ffmpeg',
        '-loglevel',
        'quiet',
        '-i',
        f'{file_path}.opus',
        '-ar',
        str(SAMPLE_RATE),
        '-ac',
        '1',
        '-f',
        's16le',
        '-'
    ],
        stdout=PIPE
    )
    data = process.stdout.read()
    start_time = time.time()
    rec.AcceptWaveform(data)
    lead_time = time.time() - start_time

    result = rec.FinalResult().lower()[14:-3]
    correctness_of_recognition = result == text
    process.terminate()
    process.kill()

    return [text, result, correctness_of_recognition, lead_time]


def main() -> None:
    """Performs basic logic. Search for files in a directory, run through automatic recognition and data collection.

    Returns:
        None

    """
    if os.path.exists(RESULT_DIR):
        shutil.rmtree(RESULT_DIR)

    os.mkdir(RESULT_DIR)

    df = pd.DataFrame(columns=['text', 'audio_text', 'correctness_of_recognition', 'time_recognition'])
    files = set(
        [os.path.join(path, name).split('.')[0] for path, subdirs, files in os.walk(PATH_DIR) for name in files]
    )
    files = sorted(files)

    for i, v in enumerate(tqdm(files), start=1):
        df.loc[i] = audio_recognition(v)

        if not i % 10000:
            df.to_csv(f'test_vosk_api_{i}.csv', index=False)

    df.to_csv('test_vosk_api.csv', index=False)


if __name__ == '__main__':
    main()
