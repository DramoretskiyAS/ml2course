"""VOSK API testing module.

For the tests to work, you need to download the required model for the Russian language from
the site https://alphacephei.com/vosk/models and unpack as "model" in the current folder.
"""
import os
import re
import shutil
import time
from subprocess import Popen, PIPE
from typing import List, Union

import pandas as pd
from datasets import load_dataset, load_metric
from tqdm import tqdm
from vosk import Model, KaldiRecognizer, SetLogLevel

DIR_NAME = 'asr_public_phone_calls_2'

BASE_DIR = os.path.dirname(os.path.realpath(__file__))
PATH_DIR = os.path.join(BASE_DIR, DIR_NAME)
RESULT_DIR = os.path.join(BASE_DIR, f"result_{os.path.basename(__file__).split('.')[0]}")
SAMPLE_RATE = 16000
LANG_ID = "ru"

SetLogLevel(0)

if not os.path.exists('model'):
    print(
        'Please download the model from https://alphacephei.com/vosk/models and '
        'unpack as "model" in the current folder.'
    )
    exit(1)

model = Model('model')
rec = KaldiRecognizer(model, SAMPLE_RATE)


CHARS_TO_IGNORE = [",", "?", "¿", ".", "!", "¡", ";", "；", ":", '""', "%", '"', "�", "ʿ", "·", "჻", "~", "՞",
                   "؟", "،", "।", "॥", "«", "»", "„", "“", "”", "「", "」", "‘", "’", "《", "》", "(", ")", "[", "]",
                   "{", "}", "=", "`", "_", "+", "<", ">", "…", "–", "°", "´", "ʾ", "‹", "›", "©", "®", "—", "→", "。",
                   "、", "﹂", "﹁", "‧", "～", "﹏", "，", "｛", "｝", "（", "）", "［", "］", "【", "】", "‥", "〽",
                   "『", "』", "〝", "〟", "⟨", "⟩", "〜", "：", "！", "？", "♪", "؛", "/", "\\", "º", "−", "^", "ʻ", "ˆ"]

test_dataset = load_dataset("common_voice", LANG_ID, split="test")

wer = load_metric("wer.py")  # https://github.com/jonatasgrosman/wav2vec2-sprint/blob/main/wer.py

chars_to_ignore_regex = f"[{re.escape(''.join(CHARS_TO_IGNORE))}]"

def audio_recognition(file_path: str, text: str) -> List[Union[str, str, bool, float]]:
    """Method for automatic speech recognition.

    Args:
        file_path: File path without extension.
        text: Text.

    Returns:
        A list containing the source text, text from the recognition service, whether the text was recognized
        correctly and the recognition time.

    """
    process = Popen([
        'ffmpeg',
        '-loglevel',
        'quiet',
        '-i',
        f'{file_path}',
        '-ar',
        str(SAMPLE_RATE),
        '-ac',
        '1',
        '-f',
        's16le',
        '-'
    ],
        stdout=PIPE
    )
    data = process.stdout.read()
    start_time = time.time()
    rec.AcceptWaveform(data)
    lead_time = time.time() - start_time

    text = re.sub(chars_to_ignore_regex, "", text).lower()

    result = rec.FinalResult().lower()[14:-3]
    correctness_of_recognition = result == text
    process.terminate()
    process.kill()

    return [text, result, correctness_of_recognition, lead_time]

def main() -> None:
    """Performs basic logic. Search for files in a directory, run through automatic recognition and data collection.

    Returns:
        None

    """
    if os.path.exists(RESULT_DIR):
        shutil.rmtree(RESULT_DIR)

    os.mkdir(RESULT_DIR)

    df = pd.DataFrame(columns=['text', 'audio_text', 'correctness_of_recognition', 'time_recognition'])

    for i, v in enumerate(tqdm(test_dataset), start=1):
        df.loc[i] = audio_recognition(v['path'], v['sentence'])

    df.to_csv('vosk_api_commonvoice.csv', index=False)


if __name__ == '__main__':
    main()
