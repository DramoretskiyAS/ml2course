"""Sova testing module.

For the tests to work, you need to deploy the sova service.
For instructions on how to unfold sova service, see here https://github.com/sovaai/sova-asr
"""
import os
from typing import List, Union

import pandas as pd
import requests
from tqdm import tqdm

BASE_DIR = os.path.dirname(os.path.realpath(__file__))
DIR_NAME = 'asr_public_phone_calls_2'
PATH = os.path.join(BASE_DIR, DIR_NAME)
URL = 'http://localhost:8888/asr'


def audio_recognition(file_path: str) -> List[Union[str, str, bool, float]]:
    """Method for automatic speech recognition.

    Args:
        file_path: File path without extension.

    Returns:
        A list containing the source text, text from the recognition service, whether the text was recognized
        correctly and the recognition time.

    """
    with open(f'{file_path}.txt', 'r') as file:
        text = file.read()[:-1]

    audio_data = open(f'{file_path}.opus', 'rb')

    response = requests.post(URL, files={'audio_blob': (f'{file_path}.opus', audio_data, 'application/octet-stream')})
    audio_text = response.json()['r'][0]['response'][0]['text']
    time_recognition = response.json()['r'][0]['response'][0]['time']

    correctness_of_recognition = audio_text == text

    audio_data.close()

    return [text, audio_text, correctness_of_recognition, time_recognition]


def main():
    """Performs basic logic. Search for files in a directory, run through automatic recognition and data collection.

    Returns:
        None

    """
    df = pd.DataFrame(columns=['text', 'audio_text', 'correctness_of_recognition', 'time_recognition'])
    files = set([os.path.join(path, name).split('.')[0] for path, subdirs, files in os.walk(PATH) for name in files])
    files = sorted(files)

    for i, v in enumerate(tqdm(files), start=1):
        df.loc[i] = audio_recognition(v)

        if not i % 50000:
            df.to_csv(f'test_sova_{i}.csv', index=False)

    df.to_csv('test_sova.csv', index=False)


if __name__ == '__main__':
    main()
