# Testing off-the-shelf ASR solutions

Testing the operation of various ready-made solutions for automatic speech recognition.
The following were used as ready-made solutions:
 - [vosk-api](https://github.com/alphacep/vosk-api)
 - [model Wav2Vec by jonatasgrosman](https://huggingface.co/jonatasgrosman/wav2vec2-large-xlsr-53-russian)
 - [model Wav2Vec by anton-l](https://huggingface.co/anton-l/wav2vec2-large-xlsr-53-russian)
 - [sova-asr](https://github.com/sovaai/sova-asr)

To install the required components, see the service documentation.

To check the tests, we used asr_public_phone_calls_2 data from an open dataset for 
the Russian language [open stt](https://github.com/snakers4/open_stt). 
This set must be downloaded and unpacked into the same directory where the tests are located.

## Requirements

- python 3.8
- docker 19.03
- GPU with compute capability >= 6.1 / 6 GB
- RAM: 8 GB
