"""Wav2Vec testing module.
"""
import os
import shutil
import time
from typing import List, Union

import librosa
import pandas as pd
import torch
from tqdm import tqdm
from transformers import Wav2Vec2ForCTC, Wav2Vec2Tokenizer


MODEL_ID_RU_JONATASGROSMAN = 'jonatasgrosman/wav2vec2-large-xlsr-53-russian'
MODEL_ID_RU_ANTON_L = 'anton-l/wav2vec2-large-xlsr-53-russian'

DIR_NAME = 'asr_public_phone_calls_2'

BASE_DIR = os.path.dirname(os.path.realpath(__file__))
PATH_DIR = os.path.join(BASE_DIR, DIR_NAME)
RESULT_DIR = os.path.join(BASE_DIR, f"result_{os.path.basename(__file__).split('.')[0]}")

tokenizer_jonata = Wav2Vec2Tokenizer.from_pretrained(MODEL_ID_RU_JONATASGROSMAN)
model_jonata = Wav2Vec2ForCTC.from_pretrained(MODEL_ID_RU_JONATASGROSMAN)

tokenizer_anton = Wav2Vec2Tokenizer.from_pretrained(MODEL_ID_RU_ANTON_L)
model_anton = Wav2Vec2ForCTC.from_pretrained(MODEL_ID_RU_ANTON_L)


def audio_recognition(file_path: str, jonatasgrosman: bool = False) -> List[Union[str, str, bool, float]]:
    """Method for automatic speech recognition.

    Args:
        file_path: File path without extension.
        jonatasgrosman: Indicates which model to use for recognition.

    Returns:
        A list containing the source text, text from the recognition service, whether the text was recognized
        correctly and the recognition time.

    """

    with open(f'{file_path}.txt', 'r') as file:
        text = file.read()[:-1]

    speech, _ = librosa.load(f'{file_path}.opus')

    model = model_jonata if jonatasgrosman else model_anton
    tokenizer = tokenizer_jonata if jonatasgrosman else tokenizer_anton

    start_time = time.time()
    input_values = tokenizer(speech, return_tensors='pt')
    output = model(**input_values)
    logits = output.logits.detach().cpu()
    predicted_ids = torch.argmax(logits, dim=-1)
    transcriptions = tokenizer.decode(predicted_ids[0])
    lead_time = time.time() - start_time

    correctness_of_recognition = transcriptions == text

    return [text, transcriptions.lower(), correctness_of_recognition, lead_time]


def main():
    """Performs basic logic. Search for files in a directory, run through automatic recognition and data collection.

    Returns:
        None

    """

    if os.path.exists(RESULT_DIR):
        shutil.rmtree(RESULT_DIR)

    os.mkdir(RESULT_DIR)

    df_jonatans = pd.DataFrame(columns=['text', 'audio_text', 'correctness_of_recognition', 'time_recognition'])
    df_anton = pd.DataFrame(columns=['text', 'audio_text', 'correctness_of_recognition', 'time_recognition'])
    files = set(
        [os.path.join(path, name).split('.')[0] for path, subdirs, files in os.walk(PATH_DIR) for name in files]
    )
    files = sorted(files)

    for i, v in enumerate(tqdm(files), start=1):
        df_jonatans.loc[i] = audio_recognition(v, jonatasgrosman=True)
        df_anton.loc[i] = audio_recognition(v)

        if not i % 50000:
            df_jonatans.to_csv(os.path.join(RESULT_DIR, f'test_Wav2Vec2_jonatas_{i}.csv'), index=False)
            df_anton.to_csv(os.path.join(RESULT_DIR, f'test_Wav2Vec2_anton_{i}.csv'), index=False)

    df_jonatans.to_csv(os.path.join(RESULT_DIR, f'test_Wav2Vec2_jonatas.csv'), index=False)
    df_anton.to_csv(os.path.join(RESULT_DIR, f'test_Wav2Vec2_anton.csv'), index=False)


if __name__ == '__main__':
    main()
