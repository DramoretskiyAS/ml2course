import sys

import yaml

ALL = "all"
CHILDREN = "children"
HOSTS = "hosts"
GROUP = "group"
PARENT = "parent"
FILE_PATH = ""


def find_all_hosts(search_parameter: str, group_name: str):
    with open(FILE_PATH) as file:
        data = yaml.load(file, Loader=yaml.FullLoader)

    if search_parameter == GROUP:
        return dict(group_name=data[ALL][CHILDREN][group_name][HOSTS].keys())

    children = list(data[ALL][CHILDREN][group_name][CHILDREN].keys())

    hosts_info = {}

    for child in children:
        children_data = data[ALL][CHILDREN][child]

        if CHILDREN in children_data.keys():
            children += list(children_data[CHILDREN].keys())
            continue

        hosts_info.update({child: children_data[HOSTS].keys()})

    return hosts_info


if __name__ == "__main__":
    parameter = input("looking for a parent or group?\n")
    group_name = input("input group\n")
    print()

    if parameter not in [PARENT, GROUP]:
        print("Invalid parameter entered.")
        sys.exit(1)

    hosts = find_all_hosts(parameter, group_name)

    for key, value in hosts.items():
        print(f"number hosts for {key} {len(value)}")
        print("\n".join(value))
        print("\n")
